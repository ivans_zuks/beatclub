'use strict';

const { sanitizeEntity } = require('strapi-utils');

/**
 * A set of functions called "actions" for `opportunity`
 */

module.exports = {
  find: async (ctx, next) => {
    const entity = await strapi.services.opportunity.find();

    return sanitizeEntity(entity, { model: strapi.models.opportunity });
  }
};
